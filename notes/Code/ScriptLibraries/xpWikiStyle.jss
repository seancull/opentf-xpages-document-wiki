/**
(c) IBM Corp., 2009 Licensed Materials
 */

/**
Licensed to OpenNTF under one or more contributor license agreements.

See the NOTICE file distributed with this work for additional information
regarding copyright ownership.  OpenNTF licenses this file
to you under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. 

*/

// Wiki Formatting/Styling Class

wikiFormating = function() {

//Import helper package
importPackage(com.ibm.domino.xsp.wiki);	

// private class members
	db:						NotesDatabase 	= null;
	subjectView:			NotesView 		= null;
			
	strHTML:				String 			= null;
	noWiki:					Array 			= [];	
	sourceCode:				Array 			= [];	
	sourceCodeCommands:		Array 			= [];				
	var url									= "";
	var LINE_BREAK							= "\r\n";
	var wikiurl = "";
	
	//Instantiate java wiki helper class
	var wikiP:				wikiParser		= new wikiParser();
											
	//-----------------
	this.prototype.setHTML = function(inHTML) {
	//-----------------
	
		try {
			strHTML = new String(inHTML.toString());		
			
			db = database;
			subjectView = db.getView("dx");
			noWiki=new Array();
			sourceCode=new Array();
			sourceCodeCommands=new Array();
						
			//url=@LeftBack(database.getHttpURL(),"/")+facesContext.getExternalContext().getRequestContextPath();
			wikiurl = applicationScope.get("wikiurl");
			//print ("this is the wiki URL: " + wikiurl);
			if(wikiurl !== ""){
				url= wikiurl+facesContext.getExternalContext().getRequestContextPath();
			}else{
				url=@LeftBack(database.getHttpURL(),"/")+facesContext.getExternalContext().getRequestContextPath();
			}
			
			//print("the REAL URL is: " + url);
											
									
		} catch (e) {
			print (e);
		} 
	}
		
	//--------------------------------
	this.prototype.getHTML = function(contentType) {
	//--------------------------------
	try {

	//Clear Font Tags
	strHTML=this.clearFontTags(strHTML);	
	
	//No Wiki - Remove
	strHTML=this.noWikiRemove(strHTML);
	
	//Source Code Process and Remove
	strHTML=this.sourceRemove(strHTML);
	
	//Process stored source code
	this.processSourceCode();
	
	//Pre Process HTML to help further parsing
	strHTML=this.preProcess(strHTML);
	
	//Process Images
	strHTML=this.images(strHTML);
				
	//Process Headings
	strHTML=this.headings(strHTML);
	
	//Process Table of Content
	if(contentType !== "books"){
		strHTML=this.tableOfContents(strHTML);	
	}	
		
	//Lists
	strHTML=this.lists(strHTML);		
		
	//Process Links
	strHTML=this.links(strHTML);
	
	//Tables
	strHTML=this.tables(strHTML);
	
	//Basic Wiki Markup
	strHTML=this.basicMarkup(strHTML);	
						
	//Process HTML
	strHTML=this.html(strHTML);	
					
	//Process Source Code
	strHTML=this.sourceReturn(strHTML);
	
	//No Wiki - Return
	strHTML=this.noWikiReturn(strHTML);	
	
		
	return strHTML;	
	
	} catch (e) {
			print (e);
		}
	}
	
	
	//--------------------------------
	this.prototype.noWikiRemove = function(inHTML:String) {
	//--------------------------------
	try {

	//Isolates no wiki blocks and removes from parsed content
	//We will put back together at end
	//Saves adding the no wiki to each regex
	var changeDollar = inHTML.replace(/\$/g,"&#36;").replace(/\\/g,"&#92;").replace(/\#/g,"\#").replace(/\*/g,"\*");
	var tmp=new Array();
	
	tmp=changeDollar.split("{{{");
	
	if(tmp.length>0){
		inHTML=tmp[0];
		
		//Loop through no wiki blocks, remove and mark for replacement at end	
		for(var x=1;x<tmp.length;x++){
			//Put in noWiki array
			noWiki[noWiki.length]=@Left(tmp[x],"}}}");
			//Add marker
			tmp[x]=tmp[x].replace(@Left(tmp[x],"}}}")+"}}}","@nowiki@"+x);
			
			inHTML+=tmp[x];
		}
	}	
					
	return inHTML;
	
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with No Wiki Blocks</div>"+inHTML;		

		}
	}
	
	//--------------------------------
	this.prototype.noWikiReturn = function(inHTML:String) {
	//--------------------------------
	try {

	//Returns the no wiki blocks
	//Saves adding the no wiki to each regex
	
	if(noWiki.length>0){
		for(var x=0;x<noWiki.length;x++){
			//inHTML=inHTML.replace("@nowiki@"+(x+1).toString(),noWiki[x]);
			var temp = "@nowiki@" + (x + 1);
			var rex = new RegExp(temp + /\b/)
			inHTML=inHTML.replace(rex,noWiki[x]);	
		}	
	}
					
	return inHTML;
	
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with No Wiki Blocks</div>"+inHTML;		

		}
	}
	
	//--------------------------------
	this.prototype.sourceRemove = function(inHTML:String) {
	//--------------------------------
	try {
	
	//Isolates source code and marks return location
	//Returned at end to avoid any markup processing
	
	var tmp=new Array();
	
	//tmp=inHTML.split(/(<p[\w\d\s\=\"]*>)*\{code:/);
	//inHTML=inHTML.replace(/<div>(\{(code|code:).*?\}).*?<\/div>/g,"$1");
	tmp=inHTML.split(/\{code:/);
	
		
	if(tmp.length>0){
		inHTML=tmp[0];
		
		//Loop through code blocks, remove and mark for replacement at end	
		for(var x=1;x<tmp.length;x++){
			
			//Add instructions to command array
			sourceCodeCommands[sourceCodeCommands.length]=@Left(tmp[x],"}").replace(/,/g,"@");
						
			//Put in Source Code array
			sourceCode[sourceCode.length]=@Left(tmp[x],"{code}");			
			
			//Add marker
			tmp[x]=tmp[x].replace(@Left(tmp[x],"{code}")+"{code}","@sourcecode@"+x);
			
			inHTML+=tmp[x];
		}
	}	
				
	return inHTML;
	
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Source Code Blocks</div>"+inHTML;		

		}
	}
	
	//--------------------------------
	this.prototype.processSourceCode = function() {
	//--------------------------------
	try {
		
	var tmp=new Array();	
	var y=0;
	var i=0;
	var lineNumStr="";
	var altLineStr="";
	var pHeight="";
	var pWidth="";
	var pStyle="";
	var commands=new Array();
	var genLineNumber=false;
	var line=1;

	//Processes the Source Code that is stored in Array sourceCode
	if(sourceCode.length>0){
		for(var x=0;x<sourceCode.length;x++){
			//reset line number
			line=1;
			
			//Breakup command string
			commands=sourceCodeCommands[x].split("|");			
			
			//Flags
			if(commands[1]!=null){
				if(@Contains(commands[1],"a")){
					//alternate lines
					altLineStr="class=\"alt\""					
				}
				if(@Contains(commands[1],"n")){
					//line numbering
					genLineNumber=true;
				}					
			}
						
			//Remove opening }
			sourceCode[x]=@Right(sourceCode[x],"}");
						
			//Split into individual lines					
			tmp=sourceCode[x].split(/\r\n|<br>|<div>/);
						
			if(tmp.length>0){
				sourceCode[x]="";
							
				for(y=0;y<tmp.length;y++){
					
					//Do not include opening and closing lines if empty					
					if(!((y==0||y==(tmp.length-1))&&(tmp[y]==""||tmp[y]=="</p>"))){
											
						if(genLineNumber){lineNumStr="<span class=\"ln\">   "+line+":  </span>";}
					
						if(line%2){
							tmp[y]="<pre "+altLineStr+">"+lineNumStr+tmp[y]+"</pre>\n";
						}else{
							tmp[y]="<pre>"+lineNumStr+tmp[y]+"</pre>\n";
						}					
					
						//Add back and remove IE closing </p>					
						sourceCode[x]+=tmp[y].replace(/<\/div>|<\/p>/,"");
					
						//increase line
						line++;
						}				
					}								
			}				
			
			//height
			if(commands[2]!=null){
				
					pHeight="height:"+commands[2]+"px;";
			}
			//width
			if(commands[3]!=null){
				
					pWidth="width:"+commands[3]+"px;";
			}			
			
			//Create style
			if(pHeight!=""||pWidth!=""){
				pStyle=" style=\""+pHeight+pWidth+"\"";
			}
			
			sourceCode[x]="<div translate=0 class=\"wikicode\""+pStyle+">"+sourceCode[x]+"</div>";				
		}	
	}	
	
	} catch (e) {			
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Source Code Blocks</div>"+inHTML;
	
		}
	}
	
	//--------------------------------
	this.prototype.sourceReturn = function(inHTML:String) {
	//--------------------------------
	try {

	//Returns the source code blocks
	
	if(sourceCode.length>0){
		for(var x=0;x<sourceCode.length;x++){
			inHTML=inHTML.replace(eval("/\\@sourcecode\\@"+(x+1).toString()+"(<\\/p>)*/"),sourceCode[x].replace(/\$/g,"&#36;"));	
		}	
	}
					
	return inHTML;
	
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Source Code Blocks</div>"+inHTML;		

		}
	}	
			
			
	//--------------------------------
	this.prototype.links = function(inHTML:String) {
	//--------------------------------
	
	//Processes Links
	//Need to do hard way as regex with functions do not seem to be working
	
	try {
	
	//First step to clean up Notes client interference	
	inHTML=inHTML.replace(/\[\[<a href=\"([\w\d:#@%\/;$()~_?\+-=\\\.&]*)\|?([\w\d]*)\">/g,"[[");
	
	//Keep this regex for future use
	//inHTML=inHTML.replace(/\[\[(https?|ftp|notes):\/\/([\w\d:#@%\/;$()~_?\+-=\\\.&]*)\|?([\w\d]*)\]\]/g,"<a href=\"$1:\/\/$2\" title=\"$3\">$3</a>");
			
	//Need to clean this up when time allows
	var a:	Array = [];	
	a=inHTML.split("[[");
	var x = 0;
	var t = "";
	var t2= "";
	var t3="";
	var tlink = "";
	var im = "";
	var preLink="";
	var ut="";
	
	if(a.length>1){
		inHTML=a[0];
		
		for (x=1; x <a.length; x++) {
			t=@Left(a[x],"]]");		
						
			if(@Contains(@LowerCase(t),"http://") || @Contains(@LowerCase(t),"https://")){
			//External Link	
			
				//Second part of cleaning up notes interference
				t=t.replace("</a>","");
			
				t2=t;
				im="<img src=\""+url+"/external.gif\" alt=\"external link\" />";				
				
				if(@Left(t,"|")!=""){
					t2=@Right(t,"|");
					t=@Left(t,"|");					
				}
				
				inHTML+="<a href=\""+t+"\" title=\""+t2+"\">"+t2+im+"</a>"+@Right(a[x],"]]");		
			
			}else{
			
			//Need to encode and escape some characters for page url/name 
            //@ReplaceSubstring in use for 8.5 backwards compatibility 
            
			//Internal link
			//Remove any title	
			t2=@Trim(@Right(t,"|"));
			if(t2!=""){
				t=@Trim(@Left(t,"|"));
			}	
			if(this.checkForDoc(t)){
				//page exists - open
				var temp = this.makeSubject(t);
				//print("temp 1 = " + temp);
				temp = java.net.URLEncoder.encode(temp);
				//print("temp 2 = " + temp);
				tlink=url+"/dx/"+ temp;
				
				im="";
				if(t2!=""){
					t=t2;
				}else{
					t2=t;
				}
			}else{
				//page does not exist - create new link	
				t3=java.net.URLEncoder.encode(@ReplaceSubstring(t,"\\","#92").replace(/\//g,"#47").replace(/&quot;/g,"#34").replace(/&amp;/g,"#38"), "UTF-8");				
				
				tlink=url+"/editpage.xsp?action=newDocument&amp;subject="+t3;
				im="<img src=\""+url+"/plus.gif\" alt=\"Create New Article\"/>";				
				
				t2="Create New Article: ";
			}
						
			inHTML+="<a href=\""+tlink+"\" title=\""+t2+"\">"+t+im+"</a>"+@Right(a[x],"]]");		
			}
		}
	}
				
	return inHTML;
	
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Links</div>"+inHTML;		

		}
	}	
		
	//--------------------------------
	this.prototype.checkForDoc = function(strSubject) {
	//--------------------------------
	try {		
		//print("strSubject = " + strSubject);
		strSubject=strSubject.replace(/^\s*|\s*$/g,'').replace(/[\/\\\\:@'"?=*%$~;+]/g,"").replace(/&quot|&lt|&gt/g,"").replace(/&amp/g,"").replace(/&#36/g,"").replace(/ /g,"_");
		//print("strSubject_mod = " + strSubject);	
		var doc:NotesDocument=subjectView.getDocumentByKey(strSubject,true);
					
		if(doc==null){
			return false;		
		}else{
			return true;
		}				
			
		} catch (e) {
			print (e);
		}
	}		
	
	
	//--------------------------------
	this.prototype.basicMarkup = function(inHTML:String) {
	//--------------------------------
	try {		
	//Converts basic markup that can be any part of content outside of nowiki	
		
	inHTML=inHTML.replace(/\*\*([^\*~]*((\*(?!\*)|~(.|(?=\n)|$))[^\*~]*)*)(\*\*|\n|$)/g,"<strong>$1</strong>");
	inHTML=inHTML.replace(/[^:]\/\/([^\/~]*((\/(?!\/)|~(.|(?=\n)|$))[^\/~]*)*)(\/\/|\n|$)/g,"<i>$1</i>");
	inHTML=inHTML.replace(/\_\_([^=\_]*)\_\_/g,"<u>$1</u>");
	inHTML=inHTML.replace(/##([^=#]*)##/g,"<tt>$1</tt>");
	inHTML=inHTML.replace(/\^\^([^=\^]*)\^\^/g,"<sup>$1</sup>");
	inHTML=inHTML.replace(/\,\,([^=\,]*)\,\,/g,"<sub>$1</sub>");	
	inHTML=inHTML.replace(/-{4,}/g,"<hr />");				
		
	return inHTML;
		
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Basic Markup</div>"+inHTML;	
		}
	}		
	
	//--------------------------------
	this.prototype.tables = function(inHTML:String) {
	//--------------------------------
	try {
//print("RAW HTML --- " + inHTML);
	inHTML=inHTML.replace(/\$/g,"%dollar%")
	inHTML=inHTML.replace(/<\/div><div>\|/g,"<br>\|");	
	//inHTML=inHTML.replace(/(^(?:\||\|=).*?)<br>$/g,"$1");
	var tmp=new Array();
	var x=0;
	//print("CLEAN HTML --- " + inHTML);
	//Identify rows
	inHTML=inHTML.replace(/(^|\n|\t|<div>|<br>|<br \/>|<p[\w\d\s\=\"]*>)\|(\=)*([\w\d\s()\&\"\=\|,\.\/+*?^%:;-]*)(<\/p>|\<br \/>|<br>)*/g,"#<tr>|$2$3</tr>");
	inHTML=inHTML.replace(/(#<tr>.*?<\/tr>).{0,2}(<\/div>|<br>|<br \/>)/g,"$1")
	
	//print("ROWS indetified --- " + inHTML);
	//Split Rows
	
	tmp=inHTML.split("#<tr>");
	
	if (tmp.length>0){		
		
		inHTML=tmp[0];
		//Found rows
		
		//Headings
		for(x=1;x<tmp.length;x++){
			tmp[x]="<tr>"+tmp[x].replace(/\|\s{0,1}\=([\s\w\d]*)/g,"<th>$1</th>");		
		}
		
		//Cells
		for(x=1;x<tmp.length;x++){
		
		//Opening <table>
		if(x==1){
			tmp[x]="<table class=\"wikitable\">"+tmp[x];
		}else{
		 if(!@Contains(@Right(tmp[x-1],7),"</tr>")){
			tmp[x]="<table class=\"wikitable\">"+tmp[x];		
		 }}
		
		//tmp[x]=tmp[x].replace(/\|([\s\w\d\,\.]*\n*)/g,"<td>$1</td>");
		tmp[x]=tmp[x].replace(/<tr>.*?<\/tr>/g,function(match){return match.replace(/\|+([^\||<]*)/g,"<td>$1</td>");});
		//tmp[x]=tmp[x].replace(/<br \/>|<br>/,"");
		
		//Closing </table>
		if(!@Contains(@Right(tmp[x],12),"</tr>")){
			tmp[x]=tmp[x].replace("</tr>","</tr></table>");
		}else if(x==(tmp.length-1)&&@Contains(@Right(tmp[x],12),"</tr>")){
			tmp[x]=tmp[x].replace("</tr>","</tr></table>");
		}
				
		inHTML+=tmp[x];		
		}						
	}
	inHTML=inHTML.replace(/%dollar%/g,"\\$")
	//print("FINAL HTML --- " + inHTML)
	return inHTML;
		
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Tables</div>"+inHTML;		

		}
	}		
	
	//--------------------------------
	this.prototype.lists = function(inHTML:String) {
	//--------------------------------
	try {		
	
	var x=0;
	
	//Only do parsing on pre identified lists					
	//Unordered Lists	
	var tmp=inHTML.split("\n@beginulist@");
	if(tmp.length>0){
		//Found
		inHTML=tmp[0];
		for(x=1;x<tmp.length;x++){
			inHTML+=wikiP.processLists(@Left(tmp[x],"@endulist@"),"ul","*")+@Right(tmp[x],"@endulist@");			
		}		
	}
	
	//Ordered Lists	
	tmp=inHTML.split("\n@beginolist@");
	if(tmp.length>0){
		//Found
		inHTML=tmp[0];
		for(x=1;x<tmp.length;x++){
			inHTML+=wikiP.processLists(@Left(tmp[x],"@endolist@"),"ol","#")+@Right(tmp[x],"@endolist@");			
		}		
	}
	
	return inHTML;
		
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Lists</div>"+inHTML;	
		}
	}	
	
	//--------------------------------
	this.prototype.headings = function(inHTML:String) {
	//--------------------------------
	//Replaces wiki headings with html equivs
	//Uses helper class to get around regex issues in 8.5 and 8.51
		
	try {				
		
	//For browser created content (ie) need to catch a header in first line as no newline or break
	if(@Left(inHTML,2)=== "=="){
		inHTML="\n"+inHTML;
	}	
		
	return wikiP.processHeadings(inHTML);	
		
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Headings</div>"+inHTML;		
	} 
	}		
	
	//--------------------------------
	this.prototype.tableOfContents = function(inHTML:String) {
	//--------------------------------
	try {	
	
	var outHTML="";
	var tmp=new Array;
	var lastLevel=2;
	var thisLevel=0;
	var strHeading="";
	var strNumber
	var y=0;
	
	var L2=1;
	var L3=1;
	var L4=1;
	
	
	tmp=inHTML.split("docHeading\"><h");
	
	if(tmp.length>1){
				
		outHTML+="<div id=\"tocHidden\" class=\"tocWrapper\"><span id=\"tocShow\" class=\"tocHide\">Show</span>";
		outHTML+="<span class=\"tocHead\">"+STRING_TOC+"</span></div>";	
	
		
		outHTML+="<div id=\"tocVisible\" class=\"tocWrapper\"><span id=\"tocHide\" class=\"tocHide\">Hide</span>";
		outHTML+="<span class=\"tocHead\">"+STRING_TOC+"</span><ul class=\"ulWrapper\">";	
	
		for(var x=1;x<tmp.length;x++){	
			thisLevel=parseInt(@Left(tmp[x],1));
			strHeading=@Right(@Left(tmp[x],"</h"+thisLevel),thisLevel+">");
			nextLevel=parseInt(@Left(tmp[x + 1],1))			
			if(thisLevel>lastLevel){
				// New group
				outHTML+="<ul>";
			}			
			if(thisLevel<lastLevel){
				// Group Ended
				for(y=(lastLevel-thisLevel);y>0;y--){
					outHTML+="</ul>";					
				}				
			}
			
			//Build number
			if(thisLevel==2){
				strNumber=L2.toString();				
			}else if(thisLevel==3){
				strNumber=(L2-1).toString()+"."+L3.toString();
			}else if(thisLevel==4){
				strNumber=(L2-1).toString()+"."+(L3-1).toString()+"."+L4.toString();
			}
			
			//Increase number
			if(thisLevel==2){
				L2++;
				L3=1;
				L4=1;			
			}else if(thisLevel==3){
				L3++;
				L4=1;
			}else if(thisLevel==4){
				L4++;
			}
			
			
			//Output list item
			outHTML+="<li>"+"<a href=\"#"+java.net.URLEncoder.encode(strHeading, "UTF-8")+"\">"+strNumber+" "+strHeading+"</a>";
			if(nextLevel < thisLevel){
				"</li>"
			}			
			lastLevel=thisLevel;
		
		}
		
		outHTML+="</ul></div>"	
	}
		
	return outHTML+inHTML;
	
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Table of Contents</div>"+outHTML+inHTML;		

		}
	}			
	
	//--------------------------------
	this.prototype.clearFontTags = function(inHTML:String) {
	//--------------------------------
	try {
	
	//No font tags please
	//Later will have option to replace with span/style combination
	var pat="/<font (.*?)>/g";
	var pato=eval(pat);
	
	inHTML=inHTML.replace(pato,"");
	
	pat="/<\\/font>/g"
	pato=eval(pat);
	
	inHTML=inHTML.replace(pato,"");		
		
	return inHTML;			
	
	} catch (e) {
			print (e);
		}
	}		
	
	//--------------------------------
	this.prototype.images = function(inHTML:String) {
	//--------------------------------
	try {
			
	//Page Images {{image.jpg|title|html attr}}
	inHTML=inHTML.replace(/\{\{([^\|]*)\|([^}\|]*)\|*([^}]*)\}\}/g,"<img src=\""+url+"/"+document1.getDocument().getUniversalID()+"/\\$file/$1\" title=\"$2\" alt=\"$2\" "+"$3"+" >");
	
	//System Images [[Media:image.jpg|title|html attrs]]
	inHTML=inHTML.replace(/\[\[Media:([^\|\]]*)\|([^}\|\]]*)\|*([^\]]*)\]\]/g,"<img src=\""+url+"/dx/$1/\\$file/$1\" title=\"$2\" alt=\"$2\" "+"$3"+" >");
			
	//Backwards compatible images [[Image:image.jpg|title|html attrs]]
	inHTML=inHTML.replace(/\[\[Image:([^\|\]]*)\|*([^}\|\]]*)\|*([^\]]*)\]\]/g,"<img src=\""+url+"/"+document1.getDocument().getUniversalID()+"/\\$file/$1\" title=\"$2\" alt=\"$2\" "+"$3"+" >");
	
	//A bit over zealous but will tighten up - replace html aligns with css
	inHTML=inHTML.replace(/align=&quot;([^&]*)&quot;/g,"class=\"image-align-$1\"");
					
	return inHTML;			
	
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with Images</div>"+inHTML;	

		}
	}		
	
	//--------------------------------
	this.prototype.html = function(inHTML:String) {
	//--------------------------------
	try {
	//Processes allowed html	
	
	if(applicationScope.htmlallow=="1"){
		inHTML=inHTML.replace(/&gt;<br \/>/g,">").replace(/&lt;/g,"<").replace(/&gt;/g,">");				
	}else{
		
	inHTML=this.allowHTML(inHTML,"blockquote");
	inHTML=this.allowHTML(inHTML,"pre");
	inHTML=this.allowHTML(inHTML,"h2");
	inHTML=this.allowHTML(inHTML,"h3");
	inHTML=this.allowHTML(inHTML,"h4");
	inHTML=this.allowHTML(inHTML,"h5");
	inHTML=this.allowHTML(inHTML,"b");	
	inHTML=this.allowHTML(inHTML,"i");	
	
	//manual links
	inHTML=inHTML.replace(/&lt;a /g,"<a ");
	inHTML=inHTML.replace(/&lt;\/a&gt;/g,"</a>");		
	
	//Line Breaks
	inHTML=inHTML.replace(/&lt;br[\s]?\/&gt;/g,"<br />");
	
	}
	
	//Quotes
	inHTML=inHTML.replace(/&quot;/g,"\"");	
	
	//remove htmlblock
	inHTML=inHTML.replace(/&lt;htmlblock&gt;/g,"");	
	inHTML=inHTML.replace(/&lt;\/htmlblock&gt;/g,"");
	
	//line feeds
	inHTML=inHTML.replace(/>>/g,"<div style=\"width:43px;float:left\">&nbsp;</div>");	
			
	return inHTML;			
	
	} catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error with HTML</div>"+inHTML;	

		}
	}		
	
	//--------------------------------
	this.prototype.allowHTML = function(inHTML:String,strAllow) {
	//--------------------------------
	inHTML=inHTML.replace(eval("/&lt;"+strAllow+"&gt;/g"),"<"+strAllow+">");
	inHTML=inHTML.replace(eval("/&lt;\\/"+strAllow+"&gt;/g"),"</"+strAllow+">");	
	
	return inHTML;
	}	
		
	//-------------------------------- 
    this.prototype.makeSubject = function(strIn) { 
    //-------------------------------- 
    	//print ("strIn = " + strIn);
        strIn = strIn.replace(/ - /g,"-");    
        //@replacesubstring for 8.5 backwards compat    
        var temp = @ReplaceSubstring(strIn.replace(/^\s*|\s*$/g,'').replace(/ /g,"_").replace(/\//g,"").replace(/&quot;|&lt;|&gt;|&#36;/g,"").replace(/&amp;/g,"").replace(/[^a-zA-Z0-9_\.\(\)-]/g,""),"\\","#92");                         
        //print("strIn_mod = " + temp);
        return temp;
    }     
    
    //-------------------------------- 
    this.prototype.preProcess = function(inHTML) { 
    //-------------------------------- 
    //Do Lazy marking of lists to help with bold conflicts
	
	try{
							
	tmp=inHTML.split(/\n/);
	var o=false;
	var u=false;
	var skipnext=false;
	var notdealt=false;
	var notdealt2=true;
	var replaceStr="\n";
		
	inHTML="";
		
	for(var x=0;x<tmp.length;x++){
		
		skipnext=false;
		notdealt2=true;
		
		//Unordered
		if(!o){
		if(@Left(tmp[x],1)==="*"){
			if(u){
				//List Continues
				inHTML+=replaceStr+tmp[x];	
										
			}else{
				if(@Left(tmp[x],2)==="**"){
					//List cannot start with 2 stars so must be bold
					inHTML+=replaceStr+tmp[x];
					
					skipnext=true;
				}else{
					inHTML+=replaceStr+"@beginulist@"+tmp[x];
					
					//List begin
					u=true;
				}
			}			
		}else{
			if(u){
				//Close List
				inHTML+=replaceStr+"@endulist@"+tmp[x];
				notdealt=false;
				notdealt2=false;
				u=false;
			}else{
				notdealt=true;	
					
			}			
		}}
		
		//Ordered
		if(!u && !skipnext){
					
		if(@Left(tmp[x],1)==="#"){
			if(o){
				//List Continues
				inHTML+=replaceStr+tmp[x];		
								
			}else{
				inHTML+=replaceStr+"@beginolist@"+tmp[x];
				
				//List begin
				o=true;				
			}
			notdealt=false;			
		}else{
			if(o){
				//Close List
				inHTML+=replaceStr+"@endolist@"+tmp[x];
				o=false;
				notdealt=false;				
			}else{
				if(notdealt2){notdealt=true};															
			}			
		}}
		
		if(notdealt){
			inHTML+="\n"+tmp[x];	
				
			notdealt=false;
		}		
	}
		
	if(o){
		inHTML+="@endolist@";
	}
	if(u){
		inHTML+="@endulist@";
	}		    
    
    return inHTML;
    
    } catch (e) {
			print (e);
			return "<div style=\"color:red\">Note: Wiki Markup Parse Error withinPre Process</div>"+inHTML;	

		}
	}		
    
    //-------------------------------- 
    this.prototype.createLog = function(strIn,strTitle) { 
    //-------------------------------- 
    	//Add create log code here
    	var doc:NotesDocument=database.createDocument();
    	doc.appendItemValue("Form","content");
    	doc.appendItemValue("subject",strTitle);
    	doc.appendItemValue("content",strIn);
    	doc.appendItemValue("categories","Log");
    	doc.computeWithForm(false,false);
    	doc.save();
    	
    }       
}