function resetScopeValues(){
	sessionScope.searchAllWords="";
	sessionScope.searchExactPhrase="";
	sessionScope.searchAnyWords="";
	sessionScope.searchNoWords="";
	sessionScope.searchBoolean="";
	sessionScope.searchTitlesOnlyEnabled="false";
	sessionScope.searchCaseSensitiveEnabled="false";
	sessionScope.searchAuthor="";
	sessionScope.searchTimeFrame="Anytime";
	sessionScope.searchTags="";
	sessionScope.communityCheck="false";
	sessionScope.documentationCheck="false";
	sessionScope.learningCheck="false";
	sessionScope.booksCheck="false";
	sessionScope.allCheck = "false";
	sessionScope.searchCategoryListBox = new Array;
	sessionScope.searchFuzzy="false";
	sessionScope.searchVariants="false";
}

function assignScopeValues(){
//	sessionScope.searchScope = "3442";
	if(viewScope.isNewScope != "true"){
		switch (sessionScope.searchScope) {
			case "standardProductDoc":
				sessionScope.documentationCheck = "true";
				break;
			case "standardLearning":
				sessionScope.learningCheck = "true";
				break;
			case "standardCommunity":  
				sessionScope.communityCheck = "true";
				break;
			case "standardBooks":
				sessionScope.booksCheck = "true";
				break;
			case "standardAll":				
				sessionScope.allCheck = "true";
				break;
// LI: WISE8JCS7Q
// Set the category to the current one.
// Set the content type to the content type of the current category.
			case "thisCategory":
				if(sessionScope.lupName){
					sessionScope.searchCategoryListBox.add(sessionScope.lupName);
				}
				if(sessionScope.typeForScoping){
					switch(sessionScope.typeForScoping){
					case "prodDoc":
						sessionScope.documentationCheck = "true";
						break;
					case "productDoc":
						sessionScope.documentationCheck = "true";
						break;
					case "other", "default":
						sessionScope.communityCheck = "true";
						break;
					case "learning":
						sessionScope.learningCheck = "true";
						break;
					case "books":
						sessionScope.booksCheck = "true";
						break;
					default:
						sessionScope.communityCheck = "true";	
					}
				}
				break;
			default:
		  		if(sessionScope.searchScope != null){
		  			if(@UserName()=="Anonymous"){
						var scopeDoc:NotesDocument;
		 				scopeDoc = database.getDocumentByID(sessionScope.searchScope);
		  			}
		  			else {
		 				var scopeDoc:NotesDocument = database.getDocumentByID(sessionScope.searchScope);
					}
				if(!(scopeDoc==null)){
					switch(scopeDoc.getItemValueString("scopeType")){
					case "productDoc":
						sessionScope.documentationCheck = "true";
						break;
					case "community":
						sessionScope.communityCheck = "true";
						break;
					case "learning":
						sessionScope.learningCheck = "true";
						break;
					case "books":
						sessionScope.booksCheck = "true";
						break;
					default:
						sessionScope.allCheck = "true";	
					}

					var sCategories = scopeDoc.getItemValue("scopeCategories");
					if(sCategories != null && sCategories.size() > 0) {
						for(i=0;i < sCategories.size();i++){
							sessionScope.searchCategoryListBox.add(sCategories[i]);
						}
					}

					var sTags = scopeDoc.getItemValueString("scopeTags")
					if(sTags != null && sTags != ""){
						sessionScope.searchTags = sTags;
					}	
					if(scopeDoc.getItemValueString("scopeAuthor")){
						sessionScope.searchAuthor=scopeDoc.getItemValueString("scopeAuthor");
					}
				}
			}
		  		break;
	  			
		}
	}
}

function doSearch(){
//	print("in doSearch");
	if(context.getUrl().toString().indexOf("searchValue") < 0){
		return;
	}
	var query;
	var straightQuery = "";
	
	if(sessionScope.centerSearchValue != null){
		query = sessionScope.centerSearchValue;
	}
	else {
	query = context.getUrlParameter("searchValue");
	}
//	print("Query === " + query);
	if (query == "Search for..."){
		query = "";
	}
	var queryArray = new Array("");
	var queryString = new String("");
	if(query.startsWith("\"") & query.endsWith("\"")){
		queryString = query;
		straightQuery += query;
//		print("xpSearch.js doSearch: in 'if(query.startsWith")
	}
	else if(query.indexOf('&')>=0 | query.indexOf('=') >=0){
		queryString = "\""+query+"\"";
		straightQuery += query;		
	}
	// MKS 2/1/11 Add parens to make sure the query is evaluated before adding the contentType scope.
	else if(query.search("AND") >= 0 || query.search("OR") >= 0 || query.search("NOT") >= 0 ||
			query.search(" and ") >= 0 || query.search("^and ") >= 0 || 
			query.search(" or ") >= 0 || 
			query.search(" not ") >= 0 || query.search("^not ") >= 0){
		if((query.search("NOT") >= 0) && (query.search("AND NOT") < 0) && !(query.startsWith("NOT"))){
			query = query.replace("NOT","AND NOT");
		}
		if((query.search(" not ") >= 0) && (query.search(" and not ") < 0)){
			query = query.replace(" not "," AND NOT ");
		}

		var hasRealHyphen = "false"; //check to see if there's a hyphen intended as a hyphen, not a boolean -
		if(query.search("- ")>=0){
			hasRealHyphen = "true";
		}

		if(hasRealHyphen == "false"){
			if(query.search("-") == 0){
				query =  query.replace("-","NOT ");
				query = query.replace("("," AND (");
			}
			else{
				query =  query.replace("-"," AND NOT ");
			}
		}
		
		if(query.search("topic" >= 0)){
			query = query.replace("topic","top?c");
		}
		
		queryString = "("+query+")";
		straightQuery += query;
	}

	else if(query.search("\\{") >= 0){
		straightQuery += query;
		query = "\""+query+"\"";
		queryString = "("+query+")";
	}

	else {
		query = query.ltrim(); //get rid of any leading space, including the one apparently substituted for a + 
		query = query.rtrim(); //get rid of any trailing space
		var hasRealHyphen = "false"; //check to see if there's a hyphen intended as a hyphen, not a boolean -
		if(query.search("- ")>=0){
			hasRealHyphen = "true";
		}	 
		//Handle literal strings in search query along with unquoted strings. MSTT8TNKNX
		var substring = "";
		var smallstring = "";
		
		if(query.indexOf("\"") >= 0){
			substring = query.substring(query.indexOf("\""),query.lastIndexOf("\"")+1);
//			print("substring: "+substring)
			smallstring = query.remove(query.indexOf("\""),(query.lastIndexOf("\"")+1) - query.indexOf("\""));
			smallstring = smallstring.ltrim(); //get rid of any leading space, including the one apparently substituted for a + 
			smallstring = smallstring.rtrim(); //get rid of any trailing space
//			print("smallstring:"+smallstring+"x");
		}
		if(substring != ""){
			queryArray = smallstring.split(/\s+/);
			queryArray.push(substring);

		}
		else{
			queryArray = query.split(/\s+/);			
		}
		for (i = 0;i < queryArray.length; i++){
			if(queryArray[i].startsWith("-") && (hasRealHyphen == "false")){
				var tmpString = queryArray[i];
				tmpString =  tmpString.replace("-"," NOT ");
				queryArray[i] = tmpString.replace("topic","top?c");
			}
			if(queryArray[i] == "|"){
				queryArray[i] = " OR ";
			}
			if(queryArray[i].search(/[,!]/) >= 0){
				var tmpString = queryArray[i];
				queryArray[i] = tmpString.replace(/[,!]/,"");
			}
			if(queryArray[i].search(/<\w/) >= 0){
				var tmpString = queryArray[i];
				queryArray[i] = tmpString.replace(/</,"\"<");
				if(tmpString.search(/\w>/) < 0){
					queryArray[i] = queryArray[i]+"\"";
				}
			}		
			if(queryArray[i].search(/\w>/) >= 0){
				var tmpString = queryArray[i];
				queryArray[i] = tmpString.replace(/>/,">\"");
				if(tmpString.search(/<\w/) < 0){
					queryArray[i] = "\""+queryArray[i];
				}
			}		
			if(queryArray[i]== "<"){
				queryArray[i] = "\"<\"";
			}		
			if(queryArray[i]== ">"){
				queryArray[i] = "\">\"";
			}		
			if(queryArray[i] == "topic"){
				queryArray[i] = "top?c";
			}

			queryString += queryArray[i];

			if(i < queryArray.length-1){
				if((queryArray[i] != "(") && (queryArray[i+1] != "|") && (queryArray[i] != " OR ") && !(queryArray[i].startsWith("NOT "))){
					queryString = queryString + " AND ";
				}
			}
		}
//print("at this point the query string is: "+queryString);

		// MKS 2/1/11 Add parens if there is a query string, to make sure the query is evaluated before adding the contentType scope.
		if (queryString != null && queryString != ""){
			straightQuery = "keywords: "+queryString; 
			queryString = "("+queryString+")";
//			print("and the query string is: "+queryString);
		}
	}
//	print("and the query string finally is: "+queryString);

		
// new stuff here

			if(sessionScope.searchTitlesOnlyEnabled == "true") {
				queryString = "(FIELD subject CONTAINS " + queryString + " OR FIELD displaySubject CONTAINS " + queryString + ")";
				straightQuery += " (titles only)";
			}
			else if(sessionScope.searchCaseSensitiveEnabled == "true") {
				queryString = "EXACTCASE " + queryString;
				straightQuery += " (exactcase)";
			}

// LI: WISE8JCS7Q
//Don't add content type when searching for "This category".
//	if(sessionScope.searchScope != "thisCategory"){
			var qstringlocal="";
			var sstringlocal="";
			
			if (sessionScope.booksCheck == "true"){
				qstringlocal = qstringlocal + "FIELD contentType CONTAINS books";
				sstringlocal = "Books";
			}
			if (sessionScope.documentationCheck == "true"){
				qstringlocal = qstringlocal + "FIELD contentType CONTAINS (productDoc OR prodDoc)";
				sstringlocal = "Product Documentation";
			}
			if (sessionScope.learningCheck == "true"){
	  			qstringlocal = qstringlocal + "FIELD contentType CONTAINS learning";
	  			sstringlocal = "Learning Center";
			}
			if (sessionScope.communityCheck == "true"){
	  			qstringlocal = qstringlocal + "(NOT FIELD contentType CONTAINS books) AND (NOT FIELD contentType CONTAINS learning) AND (NOT FIELD contentType CONTAINS productDoc) AND (NOT FIELD contentType CONTAINS prodDoc)";
	  			sstringlocal = "Community Articles"
			}
			if (sessionScope.allCheck == "true"){
	  			sstringlocal = "Entire wiki";
			}
			if (queryString !="" && qstringlocal !=""){
				queryString = queryString + " AND (" + qstringlocal + ")";
			}
			else if(queryString == ""){
				queryString = qstringlocal;
			}

			if( sstringlocal !=""){
				straightQuery += " AND content type: "+sstringlocal;
			}
//	}

	if(sessionScope.searchAuthor != null & sessionScope.searchAuthor != "" & sessionScope.searchAuthor != "Enter an Author") {
		queryString = queryString + " AND (FIELD author = \"" + sessionScope.searchAuthor + "\")";
		straightQuery += " AND author: " + sessionScope.searchAuthor;
	}
	
	var nowdate=new Date();
	var date1=new Date();
	var nowdatestr=new String("");
	var ts=new String("");

	switch (sessionScope.searchTimeFrame)
	{
		case "Anytime":
			break;
		case "Past24Hours":
			nowdate.setDate(nowdate.getDate()-1);
			break;
		case "PastWeek":
			nowdate.setDate(nowdate.getDate()-7);
			break;
		case "PastMonth":
			{
				date1.setFullYear(nowdate.getFullYear(),nowdate.getMonth(),0); //date1: the end of last month
				if (nowdate.getDate()>date1.getDate()) //today is greater than date1
					nowdate.setDate(0);
				else nowdate.setMonth(nowdate.getMonth()-1); //general
				break;
			}		
		case "PastYear":
			{
				date1.setFullYear(nowdate.getFullYear(),nowdate.getMonth(),nowdate.getDate()+1);//date1: tomorrow
				if (date1.getMonth()==2) //today is the end of Feb.
					nowdate.setFullYear(nowdate.getFullYear()-1,2,0); //the end of Feb, last year.
				else nowdate.setFullYear(nowdate.getFullYear()-1); //general
				break;
			}
				
	}

	if (sessionScope.searchTimeFrame!="Anytime" & sessionScope.searchTimeFrame!=null) {	
		nowdatestr=(nowdate.getMonth()+1)+'/'+nowdate.getDate()+'/'+nowdate.getFullYear();
		ts="[_CreationDate]>="+ nowdatestr;
		if(queryString != "" & queryString != null){
			queryString = queryString + " AND " + ts;
		}	
		else {
			queryString = queryString + ts;
		}
		straightQuery += " AND creation date: >="+ nowdatestr;
	}

	if(sessionScope.searchTags != null & sessionScope.searchTags != ""){
		if(sessionScope.searchTags.endsWith(",")){
			sessionScope.searchTags = sessionScope.searchTags.substring(0,sessionScope.searchTags.lastIndexOf(","));
		}
		tagsArray = sessionScope.searchTags.trim().split(/,/);
		var tagsString = new String;
		var tString = new String;
		var sString = new String;
		
		tagsString = "FIELD technorati CONTAINS (";
		for (i = 0; i < tagsArray.length; i++){
			if(tagsArray[i] != ""){
				tString = tString + "\"" + tagsArray[i] + "\"";
				sString += tagsArray[i];

				if(i < tagsArray.length-1){
					tString = tString + " OR "
					sString += " OR "
				}
			}
		}
		tagsString = tagsString + tString + ")";
		
		if(queryString != "" & queryString != null){
			queryString = queryString + " AND ("+tagsString+")";
		}
		else {
			queryString = "("+tagsString+")";
		}
		straightQuery += " AND tags: " + sString;

//		queryString = queryString+ " AND ("+tagsString+")";				
	}

	if((sessionScope.searchCategoryListBox != null) && (sessionScope.searchCategoryListBox.length > 0)) {
		var catString = new String;
		var categoriesString = new String;
		var categoryString = new String;
		var csString = new String;
		var cString = new String;
		var sString = new String;

		categoriesString = categoriesString + "FIELD categories CONTAINS (";
		categoryString = categoryString + " OR FIELD category CONTAINS (";

		for (i = 0; i < sessionScope.searchCategoryListBox.length; i++){
			csString = csString + "\"" + sessionScope.searchCategoryListBox[i] + "\"";
			cString = cString + "\"" + sessionScope.searchCategoryListBox[i] + "\"";
			sString += sessionScope.searchCategoryListBox[i];
			if(i < sessionScope.searchCategoryListBox.length-1){
				csString = csString + " OR ";
				cString = cString + " OR ";
				sString = sString + " OR ";
			}
		}
		categoriesString = categoriesString + csString + ")";
		categoryString = categoryString + cString + ")";

		catString = categoriesString+categoryString;

		if(queryString != "" & queryString != null){
			queryString = queryString + " AND ("+catString+")";
		}
		else {
			queryString = "("+catString+")";
		}
		straightQuery += " AND categories: " + sString;
	}
	
	if (sessionScope.searchFuzzy == "true"){
		straightQuery += " (Fuzzy search)"
	}

	if (sessionScope.searchVariants == "true"){
		straightQuery += " (Use word variants)"
	}

	viewScope.theQuery = queryString;
	viewScope.sQuery = straightQuery;
//	print("xpSearch.js.doSearch().queryString: "+queryString);
//	print("xpSearch.js.doSearch().straightQuery: "+straightQuery);
	return queryString;
}
