package com.ibm.lotus.idc.wiki;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class ConfiguredProducts {

	private Vector products=null;
	
	public ConfiguredProducts(){		
	}
	
	public void init(Vector products){
		this.products = products;
	}
	
	public List getProductListings(){
		//System.out.println("In helper file");
		List<Map<String, String>> productListing = new ArrayList<Map<String, String>>();
		Map<String, String> singleProduct=null;
		String[] tokens=null;
		String label=null, name=null, version=null, url=null, category=null; 
		
		//loop over the collection of products
		Enumeration e = products.elements();
		while(e.hasMoreElements()){
			String p = (String)e.nextElement();
			//split the values out based on the "|" token
			tokens = p.split("\\|");
			label = tokens[0].trim();
			name = tokens[1].trim();
			version = tokens[2].trim();
			url = tokens[3].trim();
			category = tokens[4].trim();
			
			//check to see if we've seen this product name before
			boolean newProduct = true;
			for(int i=0; i<productListing.size(); i++){
				if(productListing.get(i).get("name").equalsIgnoreCase(name)){
					//flag this as not a new product and assign the current index Map to singleProduct
					newProduct = false;
					singleProduct = productListing.get(i);
					break;
				}
			}
			
			//currently using a convention for denoting version/json/category combinations like so..
			//version1:jsonUrl1:category1|version2:jsonUrl2:category2|....
			// MKS 8/10/2010: Changed separator character to ~ from :
			
			//if it's a new product, then create the Map object with all the bits and pieces
			if(newProduct){
				singleProduct = new HashMap<String, String>();
				singleProduct.put("label", label);
				singleProduct.put("name", name);
				String versionUrlCategory = version + "~" + url + "~" + category;
				singleProduct.put("versionUrlCategory", versionUrlCategory);
				productListing.add(singleProduct);
			}
			//if it's not a new product, then simply concatenate this versionUrlCategory combination to the existing data
			else{
				String currentVersionUrlCategory = singleProduct.get("versionUrlCategory");
				String versionUrlCategory = currentVersionUrlCategory + "|" + version + "~" + url + "~" + category;
				singleProduct.put("versionUrlCategory", versionUrlCategory);
			}
			
		}
		
		List<String> productListingJSON = new ArrayList<String>();
		StringBuffer sb=null;
		for(Map<String, String> m : productListing){
			 sb = new StringBuffer("{");
			 sb.append("'label':'" + m.get("label") + "','name':'" + m.get("name") + "','versions':[");
			 String[] versionTokens = m.get("versionUrlCategory").split("\\|");
			 for(int i=0; i<versionTokens.length; i++){
				 String[] t = versionTokens[i].split("~");
				 sb.append("{'version':'" + t[0] + "','url':'" + t[1] + "','category':'" + t[2] + "'}");
				 if(i+1 < versionTokens.length){
					 sb.append(",");
				 }
			 }
			 sb.append("]}");
			 productListingJSON.add(sb.toString());
		}
		
		return productListingJSON;
	}

	
}
